// [Section] Exponent Operator

	// Before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

	// ES6
		const secondNum = Math.pow(8, 2);
		/*first number - base
		  second number - exponent*/
		 console.log(secondNum);


// [Section] Tempplate Literals
/*
	- Allows to write strings without using concatenation operator(+)
	- It greatly helps with code readability
*/
	let name = "John";

	// Before Template Literals
		let message = 'Hello ' + name + '! Welcome to programming.'
		console.log(message)

	// Template Literals - Uses backticks (``)
		message = `Hello ${name}!
				Welcome to programming.`
		console.log(message);

/*
	- Template literals allows us to write strings with embedded JavaScript
*/
	const interestRate = 0.1;
	const principal = 1000;
	console.log(`The interest on your savings account is: ${interestRate*principal}.`)


// [Section] Array Destructuring
/*
	- Allows us to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- It will help us with code readability

	Syntax:
		let/const [variableNameA, variableNameB, ...] = arrayName;
*/
	const fullName = ["Juan", "Dela", "Cruz"];

	// Before Array Destructuring
		console.log(fullName[0]);
		console.log(fullName[1]);
		console.log(fullName[2]);
		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

	// Array Destructuring
		const [firstName, middleName, lastName] = fullName;
		console.log(firstName);
		console.log(middleName);
		console.log(lastName);

		console.log(fullName);
		console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)


// [Section] Object Desctructuring
/*
	- Allows us to unpack properties of obejects into distinct variables

	Syntax:
		let/const {propertyNameA, propertyNameB, ...} = objectName;
*/
	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	}

	// Before the object destructuring
		console.log(person.givenName);
		console.log(person["maidenName"]);
		console.log(person.familyName);

	// Object Destructuring
		const {givenName, maidenName, familyName} = person
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);


		function getFullName({givenName, maidenName, familyName}){
			console.log(`${givenName} ${maidenName} ${familyName}`)
		}

		getFullName(person);



// [Section] Arrow Function
/*
	- It compacts alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
*/
	
	// Before ES6
		/*const hello = function(){
			console.log("Hello World!")
		}

		hello();*/

	// Arrow Function in ES6
		const hello = () => {
			console.log("Hello World!")
		}

		hello();


	// Before Arrow Function and Template Literals
		function printFullName(firstName, middleInitial, lastName){
			console.log(firstName + ' ' + middleInitial + ' ' + lastName);
		}

		printFullName("Ysa", "B.", "Bernardez");

	// Arrow Function
		let fName = (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial} ${lastName}`)
		}
		
		fName("Ysa", "B.", "Bernardez");


	// Arrow Functions with Loops
			const student = ["John", "Jane", "Judy"];

		// Before Arrow Function
			function iterate (student){
				console.log(student + " is a student");
			}
			student.forEach(iterate);

		// Arrow Function
			let students = (student) => {
					console.log(`${student} is a student`)
				}

				student.forEach(students)

			/*or*/
			student.forEach((student) => {
				console.log(`${student} is a student!`)
			})



// [Section] Implicit Return Statement
/*
	- There are instances when you can omit return statement
	- This works because even without return statement, JS implicitly adds it for the result of the function
*/

	const add = (x,y) => {
		console.log(x+y);
		return x+y;
	}

	let sum = add(23, 45);
	console.log("This is the sum contained in sum variable: ")
	console.log(sum);

	/*-----*/
	const subtract = (x,y) => x-y;
	subtract(10, 5);

	let difference = subtract(10, 5);
	console.log(difference);



// [Section] Default Function Arument Value
/*
	- Provide a default argument value if none is provided when the function is invoked.
*/

	const greet = (name = "User") => {
		return `Good morning, ${name}!`;
	}

	console.log(greet());


// [Section] Class-Based Object Blueprints
/*
	- It allows us to create or instantiation of objects using classes blueprints
*/

	// Creating a Class
		// Constructor is a special method of a class for creating or initializing an object for that class
		// Hoisted
	/*
		Syntax:
			Class className{
				constructor (objectValueA, objectValueB, ...){
					this.objectPropertyA = objectValueA;
					this.objectRpopertyB = objectValueB;
				}
			}
	*/
			class car{
				constructor(brand, name, year){
					this.carBrand = brand;
					this.carName = name; 
					this.carYear = year;
				}
			}

			let Car = new car("Toyota", "Hilux-pickup", "2015");
			console.log(Car);

			Car.carBrand = "Nissan";
			console.log(Car)

			/*Function is used before ES6 and it is not hoisted
			  Class is used in ES6 and it is hoisted*/

 